#include <iostream>
// delete variable "count"
int main(int argc, char** argv)
{
	
	int wordIndex = 1;
	int symbolIndex = 0;
	int biggerN = 0;
	int biggerIndex = 0;
	
	for(; wordIndex < argc;)
	{
		int count = 0;
		
		while (argv[wordIndex][symbolIndex] != '\0')
		{
			++count;
			++symbolIndex;
		}
		
		if (count > biggerN || count == biggerN)
		{
			biggerN = count;
			biggerIndex = wordIndex;
		}	
		
		symbolIndex = 0;
		++wordIndex;
		
	}
	std::cout << "the biggest word is " << argv[biggerIndex] << "\n";
	return 0;
	
}