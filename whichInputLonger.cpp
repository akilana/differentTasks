#include <iostream>
#include <cstring>

int main(int argc, char** argv)
{	
	if (strlen(argv[1]) > strlen(argv[2]))
	{
		std::cout << "First arg is longer: " << argv[1] << "\n";
	}
	else
	{
		if (strlen(argv[1]) == strlen(argv[2]))
		{
			std::cout << "Agrs have the same length";
		}
		else
		{
			std::cout << "Second arg longer: " << argv[2] << "\n"; 
		}
	}
	
	return 0;

}