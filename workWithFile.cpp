#include <iostream>
#include <fcntl.h> 
#include <unistd.h> 
#include <sys/types.h>
#include <sys/uio.h>
#include <cerrno>

int getData();

int main(int argc, char** argv)
{
	
	int data = getData();
	size_t dataSize = sizeof(data);
	
	int fd = open("example.txt", O_CREAT | O_WRONLY, S_IWUSR | S_IRUSR);
	
	if (fd == -1)
	{
		std::cout << "ERRNO " << errno << "\n\n\n";
		std::cout << "EACCES" << EACCES << "\n";
		std::cout << "EAGAIN" << EAGAIN << "\n";
		std:: cout << "EINVAL" << EINVAL << "\n";
 	}
	write(fd, &data, dataSize);
	
	close(fd);
	std::cout << "----------------------------------\n";
	
	fd = open("example.txt", O_CREAT | O_RDONLY);
	
	if (fd == -1)
	{
		std::cout << "ERRNO " << errno << "\n\n\n";
		std::cout << "EACCES" << EACCES << "\n";
		std::cout << "EAGAIN" << EAGAIN << "\n";
		std:: cout << "EINVAL" << EINVAL << "\n";
 	}
 	
	read(fd, &data, dataSize);
	std::cout << "Data = " << data << "\n";
	
	close(fd);
	
	return 0;
}

int getData()
{
	int data = 0;
	std::cout << "Enter a number: \n";
	std::cin >> data;
	
	return data;
}
